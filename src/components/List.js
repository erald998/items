import React, {Component} from 'react';
import { connect } from 'react-redux';
import Item from './Item'
import { Row} from 'reactstrap';
import '../assets/css/List.css'

class List extends Component {
  calculateTotal=(obj)=>{
          let  sum=0
            for (var key in obj ) {
                if (obj.hasOwnProperty(key)) {
                    sum+=Number(obj[key].price)*Number(obj[key].qty).toFixed(1)
                }
            }

        return Number(sum).toFixed(2)
   }
   renderItems=(obj)=>{
      return  Object.keys(obj).map((singleKey) =>{
                 return <Item  key={singleKey} item={obj[singleKey]} />
                 }) 
     }
    render() {
        return (
           <Row className="listContainer"  >
                 <Row className='itemList'> 
                      { this.renderItems(this.props.state.details)} 
                 </Row> 
                 <Row className="priceSum"> 
                       <h1>{ this.calculateTotal(this.props.state.details)}</h1>
                 </Row>
              
             </Row>
        );
    }
}


const mapStateToProps = (state) => {
  return({
    state:state.ItemReducer
  })
}

	

export default connect(mapStateToProps)(List);