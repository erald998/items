import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Input } from 'reactstrap';
import {addItem} from '../store/items/ItemActions'
import '../assets/css/Add.css'

class Add extends Component {
  constructor(props) {
        super(props);
        this.state ={
          name:''
        }
    }
     Change=e=>{
      this.setState({[e.target.name]:e.target.value})
    }
    Item=(e)=>{
      e.preventDefault()
      this.props.dispatch(addItem({
        product:this.state.product,
        'qty':this.state.qty,
        'price':this.state.price
      }))
      console.log(this.props.state)
    }
    render() {
        return (

        <Form className='itemForm' inline onSubmit={this.Item}>
            <FormGroup >
                <Input onChange={this.Change}  type="text" name="product" id="product" placeholder="Product name" required />
            </FormGroup>
             <FormGroup >
                <Input onChange={this.Change}  type="number" name="qty" id="qty" placeholder="Quantity" required />
            </FormGroup>
             <FormGroup >
                <Input onChange={this.Change} step="0.001" type="number" name="price" id="price" placeholder="Price" required />
            </FormGroup>
            <Button>Add</Button>
      </Form>
            
        );
    }
}


const mapStateToProps = (state) => {
  return({
    state:state.ItemReducer
  })
}

	

export default connect(mapStateToProps)(Add)