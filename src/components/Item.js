import React, {Component} from 'react'
import {Row, Col } from 'reactstrap';
import '../assets/css/Item.css'
class Item extends Component {

    render() {
    	const item=this.props.item
        return (
            <Row className='itemContainer'>
                <Col > {item.product}</Col>
                <Col > {item.qty}</Col>
                <Col >{item.price}</Col>
                <Col className='itemTotal' >{(item.qty*item.price).toFixed(3)}</Col>
            </Row>
        )
    }
}

export default Item