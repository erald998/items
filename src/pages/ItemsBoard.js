import React, {Component} from 'react'
import { Container, Row } from 'reactstrap';
import Add from '../components/Add'
import List from '../components/List'
import '../assets/css/ItemsBoard.css'
class ItemsBoard extends Component {
    render() {
        return (
            <Container className="wrapper">
            	<Row>
            		<Add />
            	</Row>
            	<Row>
            		<List />
            	</Row>

            </Container>
        );
    }
}

export default ItemsBoard