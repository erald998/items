import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'

// Import pages
import  ItemsBoard from './pages/ItemsBoard'

export default (
  <div>
    <BrowserRouter>
        <div>
           <Route exact path="/" component={ItemsBoard}/>
        </div>
    </BrowserRouter>
  </div>
)
