import { ADD_ITEM } from './ItemActions'
import initialState from '../../fakeData/initialState'



const ItemReducer = (state= initialState , action) => {
  switch (action.type) {
    case ADD_ITEM:
    let length=state.list.length
    return {
        ...state,
        details:{
            ...state.details,
            [length+1]:action.payload
        },
        list:[...state.list,length+1]
      }
  default:
      return state
}
};

export default ItemReducer;