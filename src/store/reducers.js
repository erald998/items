/**
 * Root Reducer
 */
import { combineReducers } from 'redux'

// Import Reducers
import ItemReducer from './items/ItemReducer'


// Combine all reducers into one root reducer
export default combineReducers({
  ItemReducer
  
});
